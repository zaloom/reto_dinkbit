<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\FormValidator;
use Mail;

class CotizatorController extends Controller
{
    public static $interestsTable = ["8"=>83.35,"10"=>82.86,"12"=>82.35];

    public function calculations($paymentsNumber,$amount){
        $valuesArray = array();
        $swap = array();
        #fixed values in execution
        $annualInterestRate = self::$interestsTable[''.$paymentsNumber.''];
        $fortnightInterestRate = $annualInterestRate/24;
        $fortnightIvaInterestRate = $fortnightInterestRate*1.16;
        $payment = ($amount * $fortnightIvaInterestRate/100) * pow((1 + $fortnightIvaInterestRate/100),$paymentsNumber) / (pow((1 + $fortnightIvaInterestRate/100),$paymentsNumber) - 1);

        #initializing values for first bucle
        $amortization = 0;
        $residue = $amount;

        for($i=0;$i<$paymentsNumber;$i++){
            $residue = $residue-$amortization;
            $interest = $residue*$fortnightInterestRate/100;
            $VAT = $interest*0.16;
            $amortization = $payment-$VAT-$interest;
            $paymentWithoutVAT = $payment-$VAT;
            #echo $residue.'--'.$amortization.'--'.$interest.'--'.$VAT.'--'.$payment.'--'.$paymentWithoutVAT.'<br>';
            array_push($valuesArray, array("residue"=>round($residue,2),"amortization"=>round($amortization,2),"interest"=>round($interest,2),"VAT"=>round($VAT,2),"payment"=>round($payment,2),"paymentWithoutVAT"=>round($paymentWithoutVAT,2)));
        }

        return response()->json($valuesArray);
    }

    public function validar(FormValidator $request){

            return response()->json(["mensaje"=>"pasó"]);

    }

    public function store(Request $request){
       Mail::send('emails.contact',$request->all(), function($msj){
           #$msj->from($mymail,$name=null);
            $msj->subject('Correo de Contacto');
            $msj->to('zaloompruebas@gmail.com');
        });
        return $request->email;
    }

}
