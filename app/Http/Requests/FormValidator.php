<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class FormValidator extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'firstName'=>'min:3|max:50',
            'secondName'=>'min:3|max:50',

        ];
    }

        public function messages()
    {
        return [
            'firstName.min' => 'El nombre debe contener al menos 3 caracteres',
            'firstName.max' => 'El nombre no debe sobrepasar los 50 caracteres',
            'secondName.min' => 'El apellido no debe contener menos de 3 caracteres',
            'secondName.max' => 'El nombre no debe sobrepasar los 50 caracteres'
        ];
    }

}
