<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('cotizador.inicio');
});


Route::get('/cotizador', function () {
    return view('cotizador.inicio');
});

Route::get('/cotizador/contacto', function () {
    return view('cotizador.contacto');
});

Route::get('/cotizador/cotizar', function () {
    return view('cotizador.cotizador');
});

Route::resource('mail','CotizatorController@store');

Route::get('/cotizacion/{amount},{fortnight}', 'CotizatorController@calculations');

Route::post('/cotizacion/validar', 'CotizatorController@validar');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});
