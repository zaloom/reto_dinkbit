<?php
$imgBanner='images/encabezados/header-contacto.jpg';
$text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula ultricies odio eu vulputate. Nam tellus ex, lobortis a accumsan ac, sodales eget libero. Etiam eget quam mauris. Sed porttitor ligula diam. Maecenas vel nisl at erat posuere bibendum.';
$tit='Lorem ipsum'
?>
<!DOCTYPE html>
<html lang="en" ng-app="validator">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="{{ URL::asset('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/angular.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom/controllers.js') }}"></script>
    <link href="{{URL::asset('css/general.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/reoverlay.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/contacto.css')}}" rel="stylesheet">
    <link href='{{URL::asset("//fonts.googleapis.com/css?family=Raleway")}}' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href='{{URL::asset("//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css")}}'>

</head>
<body ng-controller="validation">
@include('cotizador.menu')
@include('cotizador.banner')
<section class="section" style="padding-left:2.5vw;">
    <h2>Lorem ipsum</h2>
    <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. </p>
    <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto.  es simplemente y archivos de texto.</p>

</section>
<section class="section">
   <div class="division">
   <form id="contacto" name="Contact" novalidate>
    <input type="text" name="nombre" placeholder="Nombre" ng-pattern="/^[a-z ñáéíóú]*$/i" ng-model="firstName" required>
    <input type="text" name="apellidos" placeholder="Apellidos" ng-pattern="/^[a-z ñáéíóú]*$/i" ng-model="secondName" required>
    <input type="text" name="tel" placeholder="Teléfono casa o celular" ng-pattern="/^[0-9]{10}$/" ng-model="phone" required>
    <input type="email" name="email" placeholder="Correo electrónico" ng-model="mail" required>
    <select name="asunto" ng-model="subject" >
        <option value="" disabled selected hidden>Asunto (Elige uno)</option>
        <option value="op1">opción 1</option>
        <option value="op2">opción 2</option>
    </select>
    <input type="text" name="empresa" ng-model="company" placeholder="Empresa">
    <input type="text" name="telofi" placeholder="Teléfono oficina" ng-pattern="/^[0-9]{10}$/" ng-model="phoneOffice">
    <textarea name="mensaje" id="" cols="30" rows="10" placeholder="Mensaje" ng-model="message" required></textarea>
    <button ng-click="send(Contact);">Enviar</button>
    </form>
    </div>
    <div class="division" style="padding-top:0px;">
    <h3>Lorem ipsum</h3>
    <p class="texto"> Bosque de Ciruelos #130 int.1021
    Col. Bosque de las Lomas. Cuajimalpa de Morelos.
        México D.F. C.P. 11700</p>
    <p class="texto">
    <i class="fa fa-phone" aria-hidden="true"></i> &nbsp;&nbsp;22241607
        </p>
        <p class="texto">
    <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;contacto@dinkbit.com
    </p>
    <h3>dinkbit</h3>
    <p class="texto"><i class="fa fa-phone" aria-hidden="true"></i> &nbsp;&nbsp;22241607
        </p>
    <p class="texto">
    <i class="fa fa-envelope" aria-hidden="true"></i> &nbsp;&nbsp;hacemoscosasincreibles@dinkbit.com</p>
    <h3>síguenos en:</h3>
    <div class="icons">
    <i class="fa fa-facebook" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
    <i class="fa fa-twitter" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;
    <i class="fa fa-linkedin" aria-hidden="true"></i>
    </p>
    </div>
</section>
<section class="section">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d7526.405021657484!2d-99.2450846!3d19.4036542!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2010cbba27549%3A0xec486eedafe238bd!2sBosque+de+Ciruelos+130%2C+Bosque+de+las+Lomas%2C+11700+Ciudad+de+M%C3%A9xico%2C+D.F.!5e0!3m2!1ses!2smx!4v1462816679018" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

</section>
<script src="{{URL::asset('js/custom/scrollmenu.js')}}"></script>
@include('cotizador.footer')
</body>
</html>
