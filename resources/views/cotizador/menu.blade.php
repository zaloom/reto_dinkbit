<nav class="nav1">
   <div id="containermenu">
    <a href="{{ url('cotizador') }}"><img src="{{URL::asset('images/logos/logo.png')}}"></a>
           <ul class="menu">
           <li><a href="">quiénes somos</a></li>
            <li><a href="">nuestros servicios</a></li>
            <li><a href="">faq</a></li>
            <li><a href="{{ url('cotizador/contacto') }}">contacto</a></li>
            <li ><a id="botonmenu" href="{{ url('cotizador/cotizar') }}">cotiza tu préstamo</a></li>
        </ul>
    </div>
</nav>
<nav class="nav2">
    <div style="position:relative;">
        <a href="{{ url('cotizador') }}"><img src="{{URL::asset('images/logos/logo.png')}}"></a>
        <i id="bars" class="fa fa-bars" aria-hidden="true"></i>
    </div>
    <div class="opciones">
        <ul class="menumov">
           <li><a href="">quiénes somos</a></li>
            <li><a href="">nuestros servicios</a></li>
            <li><a href="">faq</a></li>
            <li><a href="{{ url('cotizador/contacto') }}">contacto</a></li>
            <li ><a  href="{{ url('cotizador/cotizar') }}">cotiza tu préstamo</a></li>
        </ul>
    </div>
</nav>
