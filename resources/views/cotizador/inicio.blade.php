<?php
$imgBanner='images/encabezados/header-home.jpg';
$text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula ultricies odio eu vulputate. Nam tellus ex, lobortis a accumsan ac, sodales eget libero. Etiam eget quam mauris. Sed porttitor ligula diam. Maecenas vel nisl at erat posuere bibendum. Fusce pulvinar nisl eget diam posuere, nec feugiat purus faucibus. Mauris in ipsum id metus placerat efficitur ut ac urna. Donec volutpat rutrum risus sed placerat. Integer nulla ligula, b.';
$tit='Lotem ipsum';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="{{ URL::asset('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js') }}"></script>
    <link href="{{URL::asset('css/general.css')}}" rel="stylesheet">
    <link href='{{URL::asset("//fonts.googleapis.com/css?family=Raleway")}}' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href='{{URL::asset("//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css")}}'>
</head>
<body>
@include('cotizador.menu')

@include('cotizador.banner')
<section>
    <div class="second sections">

        <h2>Lorem ipsum</h2>
<p class="texto">
    Duis posuere dictum magna, eget ullamcorper felis laoreet sit amet. Suspendisse potenti. Nulla pharetra sodales turpis, eget placerat velit suscipit ac. Quisque nunc nisl, placerat at imperdiet quis, mattis at nisl. Nunc luctus ullamcorper augue, vitae bibendum ante congue elementum. Cras vestibulum bibendum sem eu auctor. Aliquam fringilla posuere tincidunt. Proin ullamcorper purus sit amet turpis sodales, eu hendrerit velit rhoncus. Etiam ut imperdiet erat. Morbi cursus purus et nisi vulputate, eu bibendum arcu auctor. Aliquam auctor metus sem. Proin blandit luctus velit, quis consectetur libero iaculis hendrerit. Aliquam elementum ullamcorper viverra. Nam et est pharetra, gravida turpis eget, efficitur ligula. Proin feugiat augue diam, quis fringilla lectus iaculis eu.</p>


       <article id="elementos">
        <div class="elementos">
            <img style="width:8vw; margin-top:2.5vw; margin-bottom:2vw;" src="images/elementos/cotiza.png">
            <span class="texto">Lorem ipsum</span>
        </div>
        <div class="elementos elementosEspacio reloj">
            <img style="width:8vw; margin-top:1.5vw;  margin-bottom:3vw;" src="images/elementos/clock.png">
            <span class="texto">Lorem ipsum</span>
        </div>
        <div class="elementos elementosEspacio money">
        <img style="width:8vw; margin-top:1.5vw;  margin-bottom:3vw;" src="images/elementos/money.png">
        <span class="texto">Lorem ipsum</span>
        </div>
        </article>
    </div>
</section>

<section>
  <div class="third sections">
      <h2> ¿Por qué usar Lorem Ipsum?</h2>

      <div class="bloques"><h3>Primero</h3>
      <p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>
      <div class="bloques"><h3>Segundo</h3><p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>
      <div class="bloques"><h3>tercero</h3><p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>

      <div class="bloques"><h3>cuarto</h3>
      <p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>
      <div class="bloques"><h3>quinto</h3><p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>
      <div class="bloques"><h3>sexto</h3><p class="texto">Vestibulum erat dui, tincidunt id fermentum vel, accumsan in sem. Integer a sollicitudin turpis, in bibendum nunc. Proin vel lacinia sem.</p>
      </div>
    </div>
</section>
<div class="fourth sections">
<h2>Beneficios de usar lorem ipsum</h2>
<br>
<ul class="beneficios">
    <li class="vinieta"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula ultricies odio eu vulputate. Nam tellus ex, lobortis a accumsan ac, sodales eget libero.</p></li>
    <li class="vinieta"> <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula ultricies odio eu vulputate. Nam tellus ex, lobortis a accumsan ac, sodales eget libero.</p></li>
    <li class="vinieta"><p>Etiam eget quam mauris. Sed porttitor ligula diam. Maecenas vel nisl at erat posuere bibendum. Fusce pulvinar nisl eget diam posuere, nec feugiat purus faucibus. Mauris in ipsum id metus placerat efficitur ut ac urna. Donec volutpat rutrum risus sed placerat. Integer nulla ligula, blandit non molestie quis, consectetur ac ipsum. </p></li>
</ul>
    <a href="{{ url('cotizador/cotizar') }}"><button>Cotiza</button></a>
</div>
<section>

</section>

   @include('cotizador.footer')
<script src="{{URL::asset('js/custom/scrollmenu.js')}}"></script>
</body>
</html>
