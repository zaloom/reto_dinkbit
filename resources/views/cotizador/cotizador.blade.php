<?php
$imgBanner='images/encabezados/header-cotizador.jpg';
$text='Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vehicula ultricies odio eu vulputate. Nam tellus ex, lobortis a accumsan ac, sodales eget libero. Etiam eget quam mauris. Sed porttitor ligula diam. Maecenas vel nisl at erat posuere bibendum.';
$tit='Lorem ipsum'
?>
<!DOCTYPE html>
<html lang="es" ng-app="cotizator">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
    <script src="{{ URL::asset('//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/angular.min.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom/app.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/custom/controllers.js') }}"></script>
    <script type="text/javascript" src="{{ URL::asset('js/vendor/angular-rangeslider/angular.rangeSlider.js') }}"></script>
     <link href='{{URL::asset("//fonts.googleapis.com/css?family=Raleway")}}' rel='stylesheet' type='text/css'>
    <link href="{{URL::asset('js/vendor/angular-rangeslider/angular.rangeSlider.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/general.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/cotizador.css')}}" rel="stylesheet">
    <link href="{{URL::asset('css/reoverlay.css')}}" rel="stylesheet">
    <link rel="stylesheet" href='{{URL::asset("//maxcdn.bootstrapcdn.com/font-awesome/4.6.2/css/font-awesome.min.css")}}'>

</head>
<body ng-controller="CotizatorStart">
@include('cotizador.menu')
@include('cotizador.banner')
<div class="textContainer">
<section class="seccion1">
 <h2>Lorem Ipsum</h2>
 <p class="texto">Lorem Ipsum es simplemente el texto de relleno </p>
 <h3>Lorem ipsum</h3>
 <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto</p>
  <h3>Lorem ipsum</h3>
 <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto</p>
 <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto</p>
  <h3>Lorem ipsum</h3>
 <p class="texto">Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto</p>
</section>

<section class="seccion2">
    <p class="texto">Arrastre los otones de abajo para el monto que desea pedir prestado y en cuantas quincenas</p>
<hr><p class="texto">MONTO</p>
<div range-slider min="5000" max="20000" model-min="5000" model-max="slide.valueA" pin-handle="min" disabled="false" step="5000">
</div>
    <div class="values"><span class="texto izquierda">$5000</span> <span class="texto derecha">$20,000</span></div>

<div id="monto" ng-model="slide.valueA">&nbsp;<!--@{{slide.valueA | currency}}--></div>
<p class="texto">QUINCENAS</p>
<div range-slider min="8" max="12" model-min="8" model-max="slide.valueB" pin-handle="min" disabled="false" step="2" ></div>
 <div class="values"><span class="texto izquierda">8</span> <span class="texto derecha">12</span></div>
<div id="quincena" ng-model="slide.valueB">&nbsp;<!--@{{slide.valueB}}--></div>
<div style="clear:both; height:10px;"></div>



<div id="response">
   <div class="ver datos">
    <div id="datosInt">
        <div >Monto<br><span>@{{slide.valueA | currency}}</span></div>
        <div >Quincenas<br><span>@{{slide.valueB}}</span></div>
        <div >Pagos<br><span>@{{paymentShow | currency}}</div>
    </div>
    </div>
    <div id="obtenga" class="ver">Obtenga su dinero ahora</div>
    <div class="ver">Ver tabla de amortización</div>
<div id="marco">
    <div id="contenedor">
        <div class="contenidos">
            <div class="columna">No. de Pago</div>
            <div class="columna">Saldo Insoluto</div>
            <div class="columna">Amortización</div>
            <div class="columna">Interés</div>
            <div class="columna">IVA</div>
            <div class="columna">Pago</div>
        </div>
        <div class="contenidos" ng-repeat="value in results">
            <div class="columna">@{{$index+1}}</div>
            <div class="columna">@{{value.residue | currency}}</div>
            <div class="columna">@{{value.amortization  | currency}}</div>
            <div class="columna">@{{value.interest  | currency}}</div>
            <div class="columna">@{{value.VAT  | currency}}</div>
            <div class="columna">@{{value.payment  | currency}}</div>
        </div>
    </div>
</div>
</div>
</section>
</div>
<script>
    function powerSwitch(){
        angular.element($('body')).scope().getCotization( angular.element($('#monto')).scope().slide.valueA, angular.element($('#quincena')).scope().slide.valueB);
    };
</script>
@include('cotizador.footer')
<script src="{{URL::asset('js/custom/scrollmenu.js')}}"></script>
</body>
</html>
