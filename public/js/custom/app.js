/*
*
*Módulos cotizador
*
*@author: Erik Dávila
*
*
*/
var app = angular.module('cotizator', ['ui-rangeSlider'])
                    //se configura locationprovider para poder hacer uso de parametros avanzados como search o path
                    .config(function($locationProvider) {
                            $locationProvider.html5Mode({
                              enabled: true,
                              requireBase: false
                            });
                    });
var appValida = angular.module('validator', []);
