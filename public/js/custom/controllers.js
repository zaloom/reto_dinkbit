/*
*
*Controladores de cotizador
*
*@author: Erik Dávila
*
*
*/

app.controller("CotizatorStart", ["$scope", "$http", "$timeout", "$location", function ($scope, $http, $timeout, $location) {
    //initializing slider
    $scope.slide = {
        valueA: 5000,
        valueB: 8
    };

    $scope.getCotization = function (amount,paymentsNumber){

        $http.get('http://'+location.host+'/pruebasweb/retodinkbit/public/cotizacion/'+paymentsNumber+','+amount).then(function (response) {
            console.log('respuesta del servicio', response.data);
            $scope.results = response.data;
            $scope.paymentShow=response.data[0].payment;
        });
    };
     $scope.getCotization($scope.slide.valueA, $scope.slide.valueB);
}]);

appValida.controller("validation",["$scope", "$http", function($scope,$http){

    $scope.send = function(form){
        switch(true){
            case form.nombre.$error.pattern || form.nombre.$valid == false : alert('el nombre no puede contener caracteres especiales o números ni estar vacío');
                break;
            case form.apellidos.$error.pattern || form.apellidos.$valid == false: alert('el apellido no puede contener caracteres especiales o números ni estar vacío');
                break;
            case form.tel.$error.pattern || form.tel.$valid == false: alert('el teléfono es obligatorio, sólo puede contener números y una longitud de 10 digitos');
                break;
            case form.email.$valid == false: alert('el email es obligatorio y debe tener el formato correcto');
                break;
            case form.mensaje.$valid == false: alert('el mensaje es obligatorio');
                break;
            case form.telofi.$error.pattern : alert('el teléfono de oficina sólo puede contener números y una longitud de 10 digitos');
                break;
        }
        if(form.$valid){

          var peticion = {
                     method: 'POST',
                     url: 'http://'+location.host+'/pruebasweb/retodinkbit/public/cotizacion/validar',
                     params: {
                         firstName:$scope.firstName,
                         secondName:$scope.secondName
                     }
                   }

           $http(peticion).then(function (response) {
                    console.log('respuesta del servicio', response.data);

               /*Petición de envío para mail*/

                  var peticionMail = {
                     method: 'POST',
                     url: 'http://'+location.host+'/pruebasweb/retodinkbit/public/mail',
                     params: {
                         firstName:$scope.firstName,
                         secondName:$scope.secondName,
                         phone:$scope.phone,
                         email:$scope.mail,
                         subject:$scope.subject,
                         company:$scope.company,
                         phoneOffice:$scope.phoneOffice,
                         mess:$scope.message
                     }
                   }

           $http(peticionMail).then(function (response) {
                    console.log('respuesta del servicio mail', response.data);
                    alert('Tu mensaje ha sido enviado con éxto, nos pondremos en contacto a la brevedad');
                    document.getElementById("contacto").reset();
            }, function(response){
              console.log('respuesta error del servicio mail', response.data);
           });
               /*fin de petición de envío de mail*/


            }, function(response){
               $scope.mesage='';
               //console.log('respuesta error de servicio', response.data);
                angular.forEach(response.data,function(v,k){
                    $scope.mesage += v+'\n';

                });
               alert($scope.mesage);
           });
        }
        else{

        }
    }

}]);
